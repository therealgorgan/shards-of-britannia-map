# Shards of Britannia Community Map

A community-maintained map for Shards of Britannia


## How to contribute:
***Warning: Please __do not__ add player houses, vendors, or anything personal***

### 1. Obtain coordinates
You can retrieve the coordinates of your destination the following ways: 
- Click on the [map page](https://map.shardsofbritannia.com), a virtual waypoint appears at your cursor revealing the coordinates
- You can also go in-game and use the ``/where`` command, to obtain the coordinates of your character's current location
__ __
### 2. Preparing your waypoint 

Write your coordinates into a JSON entry like the following example:  
```JSON
  ,{
    "name": "Wrong",
    "type": "dungeon",
    "x": -525,
    "y": 2299
  }
  ```
The current available **types** are: ``town``, ``graveyard``, ``dungeon``, ``point_of_interest``
__ __
### 4. Add your waypoint 
Click [here](https://gitlab.com/therealgorgan/shards-of-britannia-map/-/edit/main/coordinates.json)  to edit the waypoints

Paste your new waypoint at the bottom of the list before the ending bracket ``]`` 
Don't forget the comma before your entry. The end results should look similar to: 

```JSON
[
  {
    "name": "Guardpost",
    "type": "point_of_interest",
    "x": -235,
    "y": 1427
  },
  {
    "name": "Skara Brae Graveyard",
    "type": "graveyard",
    "x": -2476,
    "y": -26
  }
]
```
__ __
### 5. Commit your change 

After you've added your waypoint(s), double check for spelling and syntax. 

When ready, click the **Commit Changes** button and your changes will be reflected after a few minutes 
__ __
***Warning: Please __do not__ add player houses, vendors, or anything personal***
